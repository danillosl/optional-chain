package io.ativa.optionalchain;

import java.lang.reflect.InvocationTargetException;
import java.util.Objects;
import java.util.function.Consumer;

import org.apache.commons.beanutils.PropertyUtils;

public class OptionalChain<T1> {

	private T1 entity;

	private OptionalChain(T1 entity) {
		this.entity = Objects.requireNonNull(entity);
	}

	public <T2> void ifPresent(Class<T2> returnType, String path, Consumer<? super T2> consumer) {
		
		try{
			T2 property = this.get(returnType, path);
			consumer.accept(property);
		}catch(IllegalStateException e){}
	}

	public static <T1> OptionalChain<T1> of(T1 entity) {
		return new OptionalChain<>(entity);
	}

	@SuppressWarnings("unchecked")
	public <T2> T2 get(Class<T2> returnType, String path) {
		Object currentEntity = entity;

		String[] names = path.split("\\.");

		String walkedPath = "";

		try {
			for (String name : names) {

				currentEntity = PropertyUtils.getProperty(currentEntity, name);

				if (currentEntity == null) {
					throw new IllegalStateException(String.format("Property %s of %s is null", walkedPath.concat(name),
							entity.getClass().getSimpleName()));
				}
				walkedPath = walkedPath.concat(name).concat(".");

			}
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}

		return (T2) currentEntity;
	}

}
